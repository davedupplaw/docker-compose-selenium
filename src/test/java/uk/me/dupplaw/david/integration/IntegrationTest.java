package uk.me.dupplaw.david.integration;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class IntegrationTest {
    @Test
    public void name() throws MalformedURLException {
        Capabilities capabilities = new ChromeOptions();
        RemoteWebDriver driver = new RemoteWebDriver(new URL("http://selenium:4444/wd/hub"), capabilities);

        driver.get("http://being_tested:3000/");

        WebElement element = new WebDriverWait(driver,10).until(
                ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".logo")));
        System.out.println( ">>>>>>>>>>>>>>>>>> " + element + "<<<<<<<<<<<<<<<<" );

//        assertThat( element.getText() ).isEqualTo( "It works!" );
    }
}
